export LOGGED_IN_USER=$( scutil <<< "show State:/Users/ConsoleUser" | awk '/Name :/ && ! /loginwindow/ { print $3 }' )
export USER_DIRECTORY='/Users/'$LOGGED_IN_USER

# Installing XCode 
xcode-select --install

# Install Homebrew
export HOMEBREW_INSTALL_FROM_API=1
/bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install.sh)"

echo '# Set PATH, MANPATH, etc., for Homebrew.' >> $USER_DIRECTORY/.zprofile
echo 'eval "$(/opt/homebrew/bin/brew shellenv)"' >> $USER_DIRECTORY/.zprofile
eval "$(/opt/homebrew/bin/brew shellenv)"

# Install Utilities
brew install --cask aldente
brew install --cask rectangle

# Installing Git Convenience Commandes
echo "\n# GIT Convenience Methods" >> $USER_DIRECTORY/.zprofile
echo "alias gb='git branch'" >> $USER_DIRECTORY/.zprofile
echo "alias gc='git checkout'" >> $USER_DIRECTORY/.zprofile
echo "alias gcb='git checkout -b'" >> $USER_DIRECTORY/.zprofile
echo "alias gstat='git status'" >> $USER_DIRECTORY/.zprofile
echo "alias gcom='git commit -m'" >> $USER_DIRECTORY/.zprofile
echo "alias gp='git push'" >> $USER_DIRECTORY/.zprofile
echo "alias gpl='git pull'" >> $USER_DIRECTORY/.zprofile

# Install Editors
brew install --cask intellij-idea
brew install --cask visual-studio-code

# Install Java Development Tooling
brew tap mdogan/zulu
brew install --cask zulu-jdk11
brew install --cask zulu-jdk17
brew install --cask zulu-jdk21
brew install jenv
brew install kotlin
brew install maven

echo "\n# JDK Convenience Methods" >> $USER_DIRECTORY/.zprofile
echo "alias j11='jenv global 11.0;exec zsh -l;java --version'" >> $USER_DIRECTORY/.zprofile
echo "alias j17='jenv global 17.0;exec zsh -l;java --version'" >> $USER_DIRECTORY/.zprofile
echo "alias j21='jenv global 21.0;exec zsh -l;java --version'" >> $USER_DIRECTORY/.zprofile

# Install GO Development Tooling
brew install go

# Install Python Development Tooling
brew install python
echo "\n# Python Alias" >> $USER_DIRECTORY/.zprofile
echo "alias python='python3"

# Install Cloud Development Tooling
brew install kubectx
brew install --cask docker
brew install --cask google-cloud-sdk
brew install k9s

# Misc Installations
brew install --cask keepassxc
brew install --cask skype

# Configure Convenience Shortcuts
echo '\n# Convenience shortcuts' >> $USER_DIRECTORY/.zprofile
echo "alias ll='ls -all'" >> $USER_DIRECTORY/.zprofile

# Cleanup
unset LOGGED_IN_USER
unset USER_DIRECTORY
unset HOMEBREW_INSTALL_FROM_API